
#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <stdio.h>
#include <string>

using namespace std;

void palabraAMorse();
void morseAPalabra();
const char letras[] = 
{'A',
'E',
'F',
'H',
'I',
'J',
'L',
'P',
'U',
'V',
'W',
'R',
'S',
'B',

'C',
'D',
'G',
'K',
'M',
'N',
'\xA5',
'O',
'Q',
'T',
'X',
'Y',
'Z'};

const char tabla[27][6] =
{{".-"},    //A
{"."},		//E
{"..-."},	//F
{"...."},	//H
{".."},		//I
{".---"},	//J
{".-.."},	//L
{".--."},	//P
{"..-"},	//U
{"...-"},	//V
{".--"},	//W
{".-."},	//R
{"..."},	//S
{"-..."},	//B

{"-.-."},	//C
{"-.."},	//D
{"--."},	//G
{"-.-"},	//K
{"--"},		//M
{"-."},		//N
{"--.--"},	//�
{"---"},	//O
{"--.-"},	//Q
{"-"},		//T
{"-..-"},	//X
{"-.--"},	//Y
{"--.."}};	//Z

int main(){
	system("cls");
	printf("Desea ingresar una palabra o un codigo? <p/c>: ");
	char introduccion;
	fflush(stdin);
	cin >> introduccion;

	if(tolower(introduccion) == 'p'){
		palabraAMorse();
	}else if(tolower(introduccion) == 'c'){
		morseAPalabra();
	}else{
		printf("Opcion invalida\n");
	}

	printf("\nRepetir? <s/n>: ");
	char repetir;
	fflush(stdin);
	cin >> repetir;
	if(tolower(repetir) == 's'){
		main();
	}
	return 0;
}

void palabraAMorse(){
	printf("\nInsertar palabra a convertir: ");
	char palabra[10];
	fflush(stdin);
	gets(palabra);

	int encontrado, pos, correcto = 1, posCodigo = 0;
	char codigo[50] = {};

	for(int i = 0; i < strlen(palabra); i++){
		palabra[i] = toupper(palabra[i]);
		encontrado = 0;
		pos = 0;
		while(encontrado != 1){
			if (palabra[i] == letras[pos]){
				for(int j = 0; j < strlen(tabla[pos]); j++){
					codigo[posCodigo] = tabla[pos][j];
					posCodigo++;
				}
				codigo[posCodigo] = ' ';
				posCodigo++;
				encontrado++;
			}
			pos++;
			if(pos > 27){
				correcto = -1;	
				encontrado++;
			}

		}
	}
	if(correcto == 1){
		cout << "La palabra " << palabra << " en morse es: " << codigo << endl;
	}else{
		cout << "Cadena invalida" << endl;
	}
}


void morseAPalabra(){

	printf("\nInsertar codigo a convertir: ");
	char codigo[50] = {}, letra[6] = {}, traduccion[20] = {};
	fflush(stdin);
	gets(codigo);
	int posLetra = 0, posTraduccion = 0, tamanoAnterior = 0, correcto = 1;
	int primero, ultimo;
	codigo[strlen(codigo)] = ' ';
	for(int i = 0; i < strlen(codigo); i++){
		tamanoAnterior = strlen(traduccion);
		if(codigo[i] != ' '){ 
			letra[posLetra] = codigo[i];
			posLetra++;
		}else{
			if(letra[0] == '.'){
				primero = 0;
				ultimo = 13;
			}else{
				primero = 13;
				ultimo = 27;
			}
			for(int j = primero; j <= ultimo; j++){
				if(strcmp(letra,tabla[j]) == 0){
					traduccion[posTraduccion] += letras[j];
					posTraduccion++;
				}
			}
			posLetra = 0;
			for(int k = 0; k < 6; k++){
				letra[k] = '\0';
			}
		}

	}
	if (correcto == 1){
		cout << "El codigo " << codigo << " traduce: " << traduccion << endl;
	}else{
		cout << "El codigo ingresado es incorrecto" << endl;
	}


}



