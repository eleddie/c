#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <iostream>
#include <Math.h>
#include <ctime>
#include <string>
#define TAM 4

void mostrarMenu();
int* vectorAleatorio(int[], int, int , char);
void accionSeleccion(char);
void seleccion(int);

using namespace std;
int main(){
	//srand(time(0));
	char opcionMenu = '0';
	while(opcionMenu != '5'){
		mostrarMenu();
		printf("Seleccion: ");
		scanf("%c", &opcionMenu);
		fflush(stdin);
		if (isdigit(opcionMenu)){
			seleccion(opcionMenu - '0');
		}else {
			printf("Opcion Invalida!\n");
			continue;
		}

	}

	return 0;
}

void mostrarMenu(){
	printf("Menu Principal:\n1-Suma\n2-Resta\n3-Multiplicacion\n4-Division\n5-Salir\n\n");
}

void seleccion(int o){
	switch (o){
	case 1: //Suma
		accionSeleccion('s');
		break;
	case 2://Resta
		accionSeleccion('r');
		break;
	case 3://Mult
		accionSeleccion('m');
		break;
	case 4://Div
		accionSeleccion('d');
		break;
	case 5://Salir

		break;
	default://Error
		printf("\nOpcion invalia.\n\n");
	}
}

void accionSeleccion(char selMenu){
	string errores = "";
	char operador, seleccion;
	int puntuacion = 0, arregloA[TAM], term1, term2;
	int* opciones;

	switch (selMenu){
	case 's':
		operador = '+';
		break;

	case 'r':
		operador = '-';
		break;

	case 'm':
		operador = '*';
		break;

	case 'd':
		operador = '/';
		break;

	}

	srand(time(0));
	for(int i = 0; i <10; i++){
		term1 = (rand()%25)+2;
		term2 = (rand()%25)+2;

		if (operador == '/' && term1 < term2) {
			int aux = term1;
			term1 = term2;
			term2 = aux;
		}

		opciones = vectorAleatorio(arregloA, term1, term2, selMenu);
		printf("\n%d) %d %c %d = ?\n",i+1,term1, operador, term2);
		for(int j = 0; j < TAM; j++){
			printf("\t%d) %d\n",j+1,opciones[j]);
		}
		do{
			printf("Opcion: ");
			scanf("%c", &seleccion);
			fflush(stdin);
		}while(isdigit(seleccion) == 0);
		int seleccionInt = seleccion - '0';

		switch (selMenu){
		case 's':
			if (opciones[seleccionInt-1] == term1 + term2){
				puntuacion++;
			}else{
				if(errores == ""){
					errores = to_string(i+1);
				}else{
					errores = errores + ", " + to_string(i+1);
				}
			}
			break;

		case 'r':
			if (opciones[seleccionInt-1] == term1 - term2){
				puntuacion++;
			}else{
				if(errores == ""){
					errores = to_string(i+1);
				}else{
					errores = errores + ", " + to_string(i+1);
				}			}
			break;

		case 'm':
			if (opciones[seleccionInt-1] == term1 * term2){
				puntuacion++;
			}else{
				if(errores == ""){
					errores = to_string(i+1);
				}else{
					errores = errores + ", " + to_string(i+1);
				}
			}
			break;

		case 'd':
			if (opciones[seleccionInt-1] == term1 / term2){
				puntuacion++;
			}else{
				if(errores == ""){
					errores = to_string(i+1);
				}else{
					errores = errores + ", " + to_string(i+1);
				}
			}
			break;

		}

	}
	printf("\nPuntuacion: %d\n", puntuacion);
	if (errores.length() > 0){
		cout << "Errores: " << errores << endl << endl;
	}
}

int* vectorAleatorio(int arregloA[], int t1, int t2, char opcion){
	srand(time(0));
	int resultado;
	if (opcion == 's'){
		resultado = t1+t2;
	}else if(opcion == 'r'){
		resultado = t1-t2;
	}else if(opcion == 'm'){
		resultado = t1*t2;
	}else if(opcion == 'd'){
		resultado = t1/t2;
	}
	int mult = 1;
	int menos = 1;
	if(resultado < 0) { menos = -1; }
	int opcionAleatoria = (rand()%3);
	int aleatorio;
	arregloA[opcionAleatoria] = resultado;
	for (int i = 0; i < TAM; i++){

		if(i == opcionAleatoria) { continue; }

		aleatorio =  menos * (rand()%(resultado+5))+1;

		if (aleatorio == resultado){
			i--;
			continue;
		}
		arregloA[i] = aleatorio;
		for(int j = 0; j < i; j++){
			if(aleatorio == arregloA[j]){
				i--;
				break;
			}
		}

	}
	return arregloA;
}