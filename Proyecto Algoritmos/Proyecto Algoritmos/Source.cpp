#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <stdio.h>
#include <math.h>

/*
EDUARDO SANCHEZ - 21.216.708
Formato de Guardado en archivo:
NOMBRE_PAIS/AREA_GEOGRAFICA/POBLACION/NOMBRE_MONEDA/TASA_DEVALUACION/DENSIDAD/
*/

//Imprime el menu y busca cuantos registros hay
void menu();
//Revisa la opcion ingresada y busca la funcion adecuada
void opcionMenu(char opcion, int &cantidad, FILE *fp);
//Anade un pais, si el pais ya existe pregunta si se quiere editar
void agregarPais(int &cantidad, FILE *fp);
//Pregunta que pais se quiere modificar y revisa si existe, si existe pasa a modificarValor
void modificarPais(int &cantidad, FILE *fp);
//Pregunta cual valor se queire modificar
void modificarValor(char pais[], char linea[], int &cantidad, FILE *fp);
//Pregunta que pais se quiere consultar
void consultarPais(int &cantidad, FILE *fp);
//Imprime la informacion del pais a consultar (Esta separado de consultarPais por razones de reciclaje)
void consultarValores(char nombrePais[], int &cantidad, FILE *fp);
//Pregunta el pais a borrar, si existe lo elimina, si solo queda 1 registro borra el archivo y lo vuelve a crear vacio
void eliminarPais(int &cantidad, FILE *fp);
//Reemplaza el valor a modificar en la cadena
void cambiarValorEnLinea(char pais[], int valor, char nuevoValor[], FILE *fin);
//Reemplaza la cadena ya modificada en el archivo
void cambiarLineaEnArchivo(char nLinea[], int lineaACambiar, float densidad, FILE *fviejo);
//Busca el nombre del pais partiendo de la linea completa
void buscarNombre(char linea[], char nombre[]);
//Convierte un arreglo de caracteres a numeros
float caracterANum(char []);
//Valida que un arreglo de caracteres sea numero (Posibilidad de decimales)
int validarDouble(char []);
//Valida que un arreglo de caracteres sea numero (Solo numeros enteros)
int validarEntero(char []);
//Regresa en valor numerico de un caracter ej. '1' -> 1
int digito(char);
//Viaja por todo el archivo imprimiendo los nombres de los paises encontrados
void listarNombres(FILE *fp);
//Revisa que el pais ingresado exista
void revisarExistencia(char linea[], char n[], char nombrePais[], int &encontrado, int &posEnArchivo, int &cantidad,FILE *fp);
//Separa los valores del formato de la linea y los guarda cada uno en su respectivo arreglo y regresa la linea de los valores (numero)
int separarValores(char pais[], char linea[], char nombre[], char area[], char poblacion[], char nomMoneda[], char tasa[], FILE *fp);
//Devuelve la misma cadena pero con la primera letra en mayuscula (Pura estetica)
void mayuscula(char []);

int main(){
	menu();
	return 0;
}

void menu(){
	FILE *fp = fopen("paises.txt","r");
	int cantidad = 0;
	if (fp != NULL){
		while(!feof(fp)){//Busca cantidad de registros
			char c, cant[50];
			if(fscanf(fp,"%c",&c)==EOF)  break;//Si el caracter siguiente es el fin del archivo se sale
			else{//Si no, pasa la linea y aumenta 1 a cantidad
				fscanf(fp, "%s", cant);
				cantidad++;
			}
		}
	}else fp = fopen("paises.txt","w");


	printf("Menu Principal:\n1.- Agregar Pais\n2.- Modificar Pais\n3.- Consultar Pais\n4.- Eliminar\n5.- Salir\n\nOpcion: ");
	char opcion;
	fflush(stdin);
	scanf("%c", &opcion);
	opcionMenu(opcion, cantidad, fp);
}

void opcionMenu(char opcion, int &cantidad, FILE *fp){
	switch(opcion){
	case '1':
		agregarPais(cantidad, fp);
		break;
	case '2':
		modificarPais(cantidad, fp);
		break;
	case '3':
		consultarPais(cantidad, fp);
		break;
	case '4':
		eliminarPais(cantidad, fp);
		break;
	case '5':
		exit(EXIT_FAILURE);
		break;
	default:
		system("cls");
		printf("Opcion Invalida\n");
		menu();
	}
}

void agregarPais(int &cantidad, FILE *fp){
	fclose(fp);
	fp = fopen("paises.txt", "a+");
	system("cls");
	printf("Insertar -1 para cancelar\nNombre del Pais: ");
	char nombre[20];
	fflush(stdin);
	gets(nombre);
	mayuscula(nombre);
	if (strcmp(nombre,"-1") == 0){
		fclose(fp);
		system("cls");
		menu();
	}

	int encontrado = 0;
	int posEnArchivo = -1;
	char linea[100], n[50] = {};
	revisarExistencia(linea, n, nombre, encontrado, posEnArchivo, cantidad, fp);
	//Si el pais que se quiere agregar ya existe
	if (encontrado == 1){
		printf("%s ya existe.\n", nombre); 
		consultarValores(nombre, cantidad, fp);
		printf("\nQue desea hacer?\n1.- Editarlo\n2.- Salir\n\nOpcion: ");
		char opcion;
		fflush(stdin);
		scanf("%c", &opcion);
		if(opcion == '1'){
			char editarLinea[100];
			modificarValor(nombre, editarLinea, cantidad, fp);
			system("cls");
			printf("Pais Modificado\n");
			menu();

		}else if(opcion == '2') {
			system("cls");
			menu();
		}else{
			system("cls");
			printf("Opcion Invalida\n");
			menu();
		}
	}

	char area[50];
	do{//Valida que la informacion ingresada sea un numero
		printf("Area Geografica del Pais (Km2): ");
		fflush(stdin);
		gets(area);
		if (strcmp(area,"-1") == 0){
			fclose(fp);
			system("cls");
			menu();
		}
	}while(!validarDouble(area) || atoi(area) == 0);

	char poblacion[50];
	do{//Valida que la informacion ingresada sea un numero
		printf("Poblacion del Pais: ");
		fflush(stdin);
		gets(poblacion);
		if (strcmp(poblacion,"-1") == 0){
			fclose(fp);
			system("cls");
			menu();
		}
	}while(!validarEntero(poblacion) || atoi(poblacion) == 0);

	printf("Nombre de la moneda: ");
	char moneda[20];
	fflush(stdin);
	gets(moneda);
	mayuscula(moneda);
	if (strcmp(moneda,"-1") == 0){
		fclose(fp);
		system("cls");
		menu();
	}

	char devaluacion[50];
	do{//Valida que la informacion ingresada sea un numero
		printf("Tasa de devaluacion con respecto al dolar americano: ");
		fflush(stdin);
		gets(devaluacion);
		if (strcmp(devaluacion,"-1") == 0){
			fclose(fp);
			system("cls");
			menu();
		}
	}while(!validarDouble(devaluacion) || atoi(devaluacion) == 0);

	if(fp == NULL)
		printf("Error al abrir el archivo");

	else{//Guarda la informacion en el archivo
		if(cantidad == 0){
			fprintf(fp,"%s/%s/%s/%s/%s/%f/",nombre,area,poblacion,moneda,devaluacion,(caracterANum(poblacion)/caracterANum(area)));
			cantidad++;
		}else{
			fprintf(fp,"\n%s/%s/%s/%s/%s/%f/",nombre,area,poblacion,moneda,devaluacion,(caracterANum(poblacion)/caracterANum(area)));
			cantidad++;
		}
	}
	fflush(stdin);
	system("cls");
	printf("%s Agregado\n\n",nombre);
	fclose(fp);
	menu();
}

void modificarPais(int &cantidad, FILE *fp){
	system("cls");
	if(cantidad == 0){
		fflush(stdin);
		system("cls");
		printf("El archivo esta vacio\n");
		menu();
	}else{
		fp = fopen("paises.txt", "r");
		printf("Que pais desea modificar?\n");
		listarNombres(fp);
		printf("Nombre del pais: ");
		char nombrePais[50];
		fflush(stdin);
		gets(nombrePais);
		rewind(fp);
		int encontrado = 0;
		int posEnArchivo = -1;
		char linea[100], n[50] = {};
		revisarExistencia(linea, n, nombrePais, encontrado, posEnArchivo, cantidad, fp);

		fclose(fp);
		if (encontrado == 0){
			system("cls");
			printf("Pais no encontrado\n");
			menu();
		}else{
			consultarValores(n,cantidad,fp);
			modificarValor(n, linea, cantidad, fp);
			system("cls");
			printf("Pais Modificado\n");
			menu();
		}
	}
}

void modificarValor(char pais[], char linea[], int &cantidad, FILE *fp){

	printf("Cual valor desea modificar?: \n1.- Nombre del pais\n2.- Area Geografica\n3.- Poblacion\n4.- Nombre de la moneda\n5.- Tasa de devaluacion\n6.- Regresar\n\nOpcion: ");
	fflush(stdin);
	char valor;
	scanf("%c", &valor);
	switch(valor){

	case '1':
		printf("Insertar -1 para cancelar\nNuevo nombre: ");
		char nombre[20];
		fflush(stdin);
		gets(nombre);
		mayuscula(nombre);
		if (strcmp(nombre,"-1") == 0){
			fclose(fp);
			system("cls");
			menu();
		}
		cambiarValorEnLinea(pais, 1, nombre, fp);
		break;

	case '2':
		printf("Nueva area Geografica del Pais: ");
		char area[20];
		fflush(stdin);
		gets(area);
		if (strcmp(area,"-1") == 0){
			fclose(fp);
			system("cls");
			menu();
		}
		cambiarValorEnLinea(pais, 2, area, fp);
		break;

	case '3':
		printf("Nueva poblacion: ");
		char poblacion[50];
		fflush(stdin);
		gets(poblacion);
		cambiarValorEnLinea(pais, 3, poblacion, fp);
		break;
	case '4':
		printf("Nueva moneda: ");
		char moneda[50];
		fflush(stdin);
		gets(moneda);
		mayuscula(moneda);
		cambiarValorEnLinea(pais, 4, moneda, fp);
		break;
	case '5':
		printf("Nueva tasa: ");
		char tasa[50];
		fflush(stdin);
		gets(tasa);
		cambiarValorEnLinea(pais, 5, tasa, fp);
		break;
	case '6':
		system("cls");
		menu();
		break;
	default:
		system("cls");
		printf("Opcion invalida\n");
		modificarPais(cantidad, fp);
		break;
	}
}

void buscarNombre(char linea[], char nombre[]){
	for(int i = 0; i<strlen(linea); i++){
		if(linea[i] != '/')
			nombre[i] += linea[i];
		else
			break;
	}
}

void cambiarValorEnLinea(char pais[], int valor, char nuevoValor[], FILE *fin){
	fclose(fin);
	fin = fopen("paises.txt", "r");
	char linea[100] = {}, nombre[50] = {}, area[50] = {}, nomMoneda[50] = {}, poblacion[50] = {}, tasa[50] = {};

	int lineaACambiar = separarValores(pais, linea, nombre, area, poblacion, nomMoneda, tasa, fin);

	float densidad;
	if (valor == 1) strcpy(nombre, nuevoValor);
	else if (valor == 2) strcpy(area, nuevoValor);
	else if (valor == 3) strcpy(poblacion, nuevoValor);
	else if (valor == 4) strcpy(nomMoneda, nuevoValor);
	else strcpy(tasa, nuevoValor);

	densidad = caracterANum(poblacion)/caracterANum(area);

	char nuevaLinea[100] = {};
	strcat(nuevaLinea, nombre);
	strcat(nuevaLinea,"/");
	strcat(nuevaLinea, area);
	strcat(nuevaLinea,"/");
	strcat(nuevaLinea, poblacion);
	strcat(nuevaLinea,"/");
	strcat(nuevaLinea,nomMoneda);
	strcat(nuevaLinea,"/");
	strcat(nuevaLinea,tasa);
	strcat(nuevaLinea,"/");
	cambiarLineaEnArchivo(nuevaLinea, lineaACambiar, densidad, fin);
}

void cambiarLineaEnArchivo(char nLinea[], int lineaACambiar, float densidad, FILE *fviejo){
	fclose(fviejo);
	fviejo = fopen("paises.txt", "r");
	FILE *fout = fopen("paisesout.txt", "w");
	int linea = 0;
	while(!feof(fviejo)){
		char lineaVieja[50];
		fscanf(fviejo, "%s", lineaVieja);
		if(linea != 0) fprintf(fout, "\n");
		if(linea == lineaACambiar){
			fprintf(fout, "%s", nLinea);
			fprintf(fout, "%f/",densidad);
		}else{
			fprintf(fout, "%s", lineaVieja);
		}
		linea++;
	}
	fclose(fviejo);
	fclose(fout);
	fviejo = fopen("paises.txt", "w");
	fout = fopen("paisesout.txt", "r");
	linea = 0;
	while(!feof(fout)){
		char lineaC[50];
		fscanf(fout, "%s", lineaC);
		if(linea != 0) fprintf(fviejo, "\n");
		fprintf(fviejo, "%s", lineaC);
		linea++;
	}
	fclose(fout);
	fclose(fviejo);
	remove("paisesout.txt");

}

void consultarPais(int &cantidad, FILE *fp){
	system("cls");
	if(cantidad == 0){
		printf("El archivo esta vacio\n");
		menu();
	}

	printf("Que pais desea consultar?\n");
	rewind(fp);
	listarNombres(fp);

	printf("Nombre del pais: ");
	char nombrePais[50];
	fflush(stdin);
	gets(nombrePais);
	rewind(fp);
	int encontrado = 0;
	int posEnArchivo = -1;
	char lineaRevisar[100], n[50] = {};
	revisarExistencia(lineaRevisar, n, nombrePais, encontrado, posEnArchivo, cantidad, fp);

	if (encontrado == 0){
		system("cls");
		printf("Pais no encontrado\n");
		fclose(fp);
		menu();
	}else{
		consultarValores(nombrePais, cantidad, fp);
		system("pause");
		system("cls");
		fclose(fp);
		menu();
	}
}

void consultarValores(char nombrePais[], int &cantidad, FILE *fp){

	char linea[100] = {}, nombre[50] = {}, area[50] = {}, nomMoneda[50] = {}, poblacion[50] = {}, tasa[50] = {};
	separarValores(nombrePais, linea, nombre, area, poblacion, nomMoneda, tasa, fp);
	printf("Nombre: \t\t%s\n", nombre);
	printf("Area Geografica(Km2): \t%s\n", area);
	printf("Poblacion: \t\t%s\n", poblacion);
	printf("Nombre de la Moneda: \t%s\n", nomMoneda);
	printf("Devaluacion (dolar): \t%s\n", tasa);
	printf("Densidad: \t\t%f\n\n", caracterANum(poblacion)/caracterANum(area));

}

void eliminarPais(int &cantidad, FILE *fp){
	system("cls");
	if(cantidad == 0){
		printf("Archivo vacio\n");
		menu();
	}
	printf("Que pais desea eliminar?\n");
	rewind(fp);
	listarNombres(fp);

	printf("Nombre del pais: ");
	char nombrePais[50];
	fflush(stdin);
	gets(nombrePais);
	rewind(fp);
	int encontrado = 0;//Se pasara por referencia
	int posEnArchivo = -1;//Se pasara por referencia
	char lineaRevisar[100], n[50] = {};
	revisarExistencia(lineaRevisar, n, nombrePais, encontrado, posEnArchivo, cantidad, fp);

	if (encontrado == 0){
		system("cls");
		printf("Pais no encontrado\n");
		fclose(fp);
		menu();
	}else{//Si se encuentra el pais que se quiere eliminar
		fclose(fp);
		fp = fopen("paises.txt", "r");
		FILE *fout = fopen("paisesout.txt", "w");
		int linea = -1;

		while(!feof(fp)){
			linea++;
			char lineaVieja[50];
			fscanf(fp, "%s", lineaVieja);
			if(linea != posEnArchivo) {
				if (linea != 0) fprintf(fout, "\n");
				fprintf(fout, "%s", lineaVieja);
			}else continue;
		}

		fclose(fp);
		fclose(fout);
		fp = fopen("paises.txt", "w");
		fout = fopen("paisesout.txt", "r");
		linea = 0;
		while(!feof(fout) && cantidad > 1){
			char lineaC[50];
			fscanf(fout, "%s", lineaC);
			if(linea != 0) fprintf(fp, "\n");
			fprintf(fp, "%s", lineaC);
			linea++;
		}
		fclose(fout);
		fclose(fp);
		remove("paisesout.txt");
		system("cls");
		printf("%s Eliminado\n\n", nombrePais);
		cantidad--;
		menu();
	}

}

int validarDouble(char c[]){
	int dec = 0;//Variable de control para validar solo 1 punto
	for(int i = 0; i < strlen(c); i++){
		if (c[i] == '.' && dec < 1) {//Si el caracter es punto se suma 1 a dec y se salta el caracter
			dec++;
			continue;
		}
		else if (isdigit(c[i]) == 0) {//Si el caracter no es numero devuelve 0. Si se encuentra mas de un punto caera aqui y dara error
			printf("Dato Invalido\n");
			return 0;
		}
	}
	return 1;
}

int validarEntero(char c[]){
	int dec = 0;
	for(int i = 0; i < strlen(c); i++){
		if (isdigit(c[i]) == 0) {//Si el caracter por donde va no es digito devuelve 0;
			printf("Dato Invalido\n");
			return 0;
		}
	}
	return 1;//Si todos son digitos devuelve 1
}

float caracterANum(char c[]){
	int pEntera = 0, pDecimal = 0, dec = 0, largoDec = 0;
	for(int i = 0; i < strlen(c); i++){
		if(c[i] != '.' && dec == 0){
			pEntera = pEntera * 10 + digito(c[i]);//Ve guardando los numeros en la variable pEntera si aun no se consigue un '.'
		}else if (c[i] == '.'){//Si se consigue el punto se salta el caracter
			dec = 1;
			continue;
		}else {//Cuando la variable dec ya es 1 se comienzan a guardar los numeros en otra variable pDecimal y se cuenta cuantos decimales hay en largoDec
			largoDec++;
			pDecimal = pDecimal * 10 + digito(c[i]);
		}
	}
	if(largoDec == 0) return (float)pEntera;//Si no se encontraron decimales solo se devuelde la parte entera
	else return (float)(pEntera + pDecimal / pow(10,largoDec));//Si se encontraron el numero sera la pEntera + pDecimal/10^LargoDec 
	//(ej: 12.34, pEntera = 12, pDecimal = 34, largoDec = 2. Devuelve 12 + 34/100 = 12.34)
}

int digito(char c){
	if (c == '0') return 0;
	else if (c == '1') return 1;
	else if (c == '2') return 2;
	else if (c == '3') return 3;
	else if (c == '4') return 4;
	else if (c == '5') return 5;
	else if (c == '6') return 6;
	else if (c == '7') return 7;
	else if (c == '8') return 8;
	else return 9;
}

void listarNombres(FILE *fp){
	while (!feof(fp)){
		char lineaListado[100] = {}, n[50] = {};
		fscanf(fp, "%s", lineaListado);
		buscarNombre(lineaListado, n);
		mayuscula(n);
		printf("- %s\n",n);
	}
}

void revisarExistencia(char linea[], char n[], char nombrePais[], int &encontrado, int &posEnArchivo, int &cantidad,FILE *fp){
	while(!feof(fp) && cantidad > 0){
		posEnArchivo++;
		char lineaW[100], nW[50] = {};
		fscanf(fp, "%s", &lineaW);
		buscarNombre(lineaW, nW);
		if(strcmpi(nombrePais,nW) == 0){
			system("cls");
			encontrado = 1;	
			strcpy(linea, lineaW);
			strcpy(n, nW);
			break;
		}
	}
}

int separarValores(char pais[], char linea[], char nombre[], char area[], char poblacion[], char nomMoneda[], char tasa[], FILE *fp){
	int lineaASeparar = -1;
	fclose(fp);
	fp = fopen("paises.txt", "r");
	while(!feof(fp)){//Busca cual es la linea del archivo que se quiere obtener los valores
		lineaASeparar++;
		char bLinea[100], n[50] = {};
		fscanf(fp, "%s", &bLinea);
		buscarNombre(bLinea, n);
		if(strcmpi(pais,n) == 0){
			strcpy(linea, bLinea);//Guarda la linea en la variable 'linea'
			break;
		}
	}

	//'iP' es la posicion del arreglo de cada dato (nombre,area,pob,moneda,tasa)
	//'i' es la posicion del arreglo de la linea completa
	int iP = 0, i = 0, separador = 0, condicion = 1;
	//La variable separador es el que controla que dato se esta leyendo y condicion es el punto donde termina cada dato (/)
	while(separador != condicion){
		if(linea[i] == '/'){//Si se consigue en '/' se aumenta condicion, separador, y se regresa iP a cero
			if(condicion != 5) condicion++;
			separador++;
			iP = 0;//'iP' vuelve a cero para emplezar en el nuevo dato desde la primera posicion
		}else{//Segun la posicion de 'separador' se decide donde se va a guardar el caracter
			if(separador == 0) nombre[iP] = linea[i];
			else if (separador == 1) area[iP] = linea[i];
			else if (separador == 2) poblacion[iP] = linea[i];
			else if (separador == 3) nomMoneda[iP] = linea[i];
			else tasa[iP] = linea[i];
			iP++;
		}
		i++;
	}
	return lineaASeparar;
}

void mayuscula(char palabra[]){
	//Regresa la primera letra en mayuscula
	for(int i = 0; i < strlen(palabra); i++){
		if (i == 0) palabra[i] = toupper(palabra[i]);
		else palabra[i] = palabra[i];
	}
}