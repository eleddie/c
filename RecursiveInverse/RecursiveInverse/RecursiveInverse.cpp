#include <stdio.h>
#include <iostream>
#include <math.h>

int flip( int n, int largo){
	if (n <= 9) return n;
	else return ((n % 10) * pow(10,largo-1)) + flip(n / 10, largo-1);
}



int largo( int numero){
	if (numero == 0) return 0;
	else return 1 + largo(numero/10);
}

int factorialI(int num){
	int fact = 1;
	for (int i = 1; i <= num; i++){
		fact = fact*i;
	}
	return fact;
}

int factorial(int num, int f){
	if (num == 0) return f;
	else return factorial(num-1, f*num);

}

int primo(int num, int div){
	if(div == 1) return 1;
	if(num % div == 0) return 0;
	else return primo(num, div-1);
}

int voltear(int num, int nuevo){
	if (num == 0) return nuevo;
	else return voltear(num/10, nuevo*10+num%10);
}


int main()
{
	printf("Insertar numero: ");
	int numero;
	scanf("%d", &numero);
	//printf("El numero %d en binario es: %d\n\n",numero, flip(numero,largo(numero)));
	//printf("Factorial de %d es %d", numero, factorial(numero,1));

	if(primo(numero,numero/2) == 1){
		printf("El numero %d es primo\n", numero);
		if(primo(voltear(numero,0),voltear(numero,0)/2) == 1){
			printf("El numero %d es omirp\n", numero);
		}else{
			printf("El numero %d no es omirp\n", numero);
		}
	}
	else printf("El numero %d no es primo\n", numero);
	
	return main();

	system("PAUSE");
	return 0;
}

