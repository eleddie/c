#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#define n 10

int aleatorio(int v[n], int final);
void llenar(int v[n]);
void imprimir(int v[n]);
int main(){
   system("cls");
   int v[n];
   llenar(v);
   imprimir(v);
   system("pause");
   return 0;
}
void llenar(int v[n]){

  srand(time(NULL));
  for(int i=0;i<n;i++){
    v[i]=aleatorio(v,i);
  }
}

void imprimir(int v[n]){
printf("\n---\n\n");
  for(int i=0;i<n;i++){
    printf("%d\n",v[i]);
  }
  
printf("\n---\n\n");
  
}

int aleatorio(int v[], int final){
int num,i, b=1;

   while (b==1){
      num=rand()%10+1;
      for (i=0;i<final;i++){
	 if (num==v[i])
	    i=final+5;
      }
      if (i==final)
	 b=0;
   }
   return num;
}